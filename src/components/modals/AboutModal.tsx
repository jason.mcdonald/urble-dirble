import { BaseModal } from './BaseModal'
import { solution } from '../../lib/words'

type Props = {
  isOpen: boolean
  handleClose: () => void
}

export const AboutModal = ({ isOpen, handleClose }: Props) => {
  let link = "https://www.urbandictionary.com/define.php?term=" + solution.replace(' ', '+')

  return (
    <BaseModal title="About" isOpen={isOpen} handleClose={handleClose}>
      <p className="text-sm text-gray-500 dark:text-gray-300">
        Urban Dictionary Word Guessing Game, based on Source -{' '}
        <a
          href="https://github.com/hannahcode/GAME"
          className="underline font-bold"
        >
          check out the code here
        </a>{' '}
      </p>
      <p className="text-sm text-gray-500 dark:text-gray-300">
        You can also cheat by viewing the urban dictionary defintion  -{' '}
        <a
          href={link}
          className="underline font-bold"
        >
          click here to cheat, i'll never tell
        </a>{' '}
      </p>
      <p className="text-sm text-gray-500 dark:text-gray-300">
        Written by Jason McDonald - jason.mcdonald11@gmail.com
      </p>
    </BaseModal>
  )
}
