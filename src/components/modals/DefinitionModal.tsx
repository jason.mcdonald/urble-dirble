import { BaseModal } from './BaseModal'
import { Definition } from '../definition/Definition'

type Props = {
  revealWord: boolean,
  isOpen: boolean
  handleClose: () => void
}

export const DefinitionModal = ({ revealWord, isOpen, handleClose }: Props) => {
  return (
    <BaseModal title="Definition(s)" isOpen={isOpen} handleClose={handleClose}>
      <Definition key={0} revealWord={revealWord} />
    </BaseModal>
  )
}
