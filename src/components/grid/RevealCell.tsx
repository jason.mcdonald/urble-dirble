import classnames from 'classnames'
import {
  ArrowDownIcon
} from '@heroicons/react/outline'

type Props = {
  id: number,
  onClick: (id: number) => void,
}

export const RevealCell = ({ id, onClick }: Props) => {
  const classes = classnames(
    'w-14 h-14 border-solid border-2 flex items-center justify-center mx-0.5 text-4xl font-bold rounded dark:text-white'
  )

  return (
    <div className={classes}>
      <ArrowDownIcon
        className="h-6 w-6 dark:stroke-white cursor-pointer"
        onClick={() => onClick(id)}
      />
    </div>
  )
}
