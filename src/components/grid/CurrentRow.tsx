import { MAX_WORD_LENGTH } from '../../constants/settings'
import { isOneWord } from "../../lib/words"
import { Cell } from './Cell'
import { DeadCell } from './DeadCell'

type Props = {
  guess: string,
}

export const CurrentRow = ({ guess }: Props) => {

  if (isOneWord()) {
    const splitGuess = guess.split('')
    const emptyCells = Array.from(Array(MAX_WORD_LENGTH - splitGuess.length))

    return (
      <div className="flex justify-center mb-1">
        {splitGuess.map((letter, i) => (
          <Cell key={i} value={letter} />
        ))}
        {emptyCells.map((_, i) => (
          <Cell key={i} />
        ))}
      </div>
    )
  }

  const firstGuess = guess.slice(0,5).split('')
  const secondGuess = guess.slice(5,10).split('')
  const firstEmptyCells = Array.from(Array(MAX_WORD_LENGTH - firstGuess.length))
  const secondEmptyCells = Array.from(Array(MAX_WORD_LENGTH - secondGuess.length))

  return (
    <div className="flex justify-center mb-1">
      {firstGuess.map((letter, i) => (
        <Cell key={i} value={letter} />
      ))}
      {firstEmptyCells.map((_, i) => (
        <Cell key={i} />
      ))}
      <DeadCell key={6} />
      {secondGuess.map((letter, i) => (
        <Cell key={i + 6} value={letter} />
      ))}
      {secondEmptyCells.map((_, i) => (
        <Cell key={i + 6} />
      ))}
    </div>
  )
}
