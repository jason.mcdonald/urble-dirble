import { MAX_WORD_LENGTH } from '../../constants/settings'
import { isOneWord } from "../../lib/words"
import { Cell } from './Cell'
import { DeadCell } from './DeadCell'

export const EmptyRow = () => {
  const emptyCells = Array.from(Array(MAX_WORD_LENGTH))

  if (isOneWord()) {
    return (
      <div className="flex justify-center mb-1">
        {emptyCells.map((_, i) => (
          <Cell key={i} />
        ))}
      </div>
    )
  }

  return (
    <div className="flex justify-center mb-1">
      {emptyCells.map((_, i) => (
        <Cell key={i} />
      ))}
      <DeadCell key={6} />
      {emptyCells.map((_, i) => (
        <Cell key={i + 6} />
      ))}
    </div>
  )
}
