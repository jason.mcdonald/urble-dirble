import {
  LockOpenIcon
} from '@heroicons/react/outline'

import classnames from 'classnames'

type Props = {
  show: boolean,
  onClick: () => void
}

export const UnlockLetter = ({
  show,
  onClick
}: Props) => {
  const classes = classnames(
    'h-6 w-6 mr-2 cursor-pointer dark:stroke-white'
  )

  return show ? (
    <div className={classes}>
     <LockOpenIcon
       className="h-6 w-6 dark:stroke-white cursor-pointer"
       onClick={onClick}
     />
   </div>
 ) : (<div className={classes}></div>)
}
