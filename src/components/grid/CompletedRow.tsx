import { getGuessStatuses } from '../../lib/statuses'
import { isOneWord } from "../../lib/words"
import { Cell } from './Cell'
import { DeadCell } from './DeadCell'

type Props = {
  guess: string,
}

export const CompletedRow = ({ guess }: Props) => {
  const statuses = getGuessStatuses(guess)

  if (isOneWord()) {
    return (
      <div className="flex justify-center mb-1">
        {guess.split('').map((letter, i) => (
          <Cell key={i} value={letter} status={statuses[i]} />
        ))}
      </div>
    )
  }

  let firstWord = guess.slice(0,5);
  let secondWord = guess.slice(5,10);

  return (
    <div className="flex justify-center mb-1">
      {firstWord.split('').map((letter, i) => (
        <Cell key={i} value={letter} status={statuses[i]} />
      ))}
      <DeadCell key={6} />
      {secondWord.split('').map((letter, i) => (
        <Cell key={i + 6} value={letter} status={statuses[i + 6]} />
      ))}
    </div>
  )
}
