import { MAX_WORD_LENGTH } from '../../constants/settings'
import { isOneWord } from "../../lib/words"
import { DeadCell } from "./DeadCell"
import { RevealCell } from "./RevealCell"

type Props = {
  show: boolean,
  unlockLetter: (id: number) => void,
}

export const RevealRow = ({ show, unlockLetter }: Props) => {
  const revealCells = Array.from(Array(MAX_WORD_LENGTH))

  if (!show) {
    return (<></>)
  }

  if (isOneWord()) {
    return (
      <div className="flex justify-center mb-1">
        {revealCells.map((_, i) => (
          <RevealCell key={i} id={i} onClick={unlockLetter} />
        ))}
      </div>
    )
  }

  return (
    <div className="flex justify-center mb-1">
      {revealCells.map((_, i) => (
        <RevealCell key={i} id={i} onClick={unlockLetter} />
      ))}
      <DeadCell key={6} />
      {revealCells.map((_, i) => (
        <RevealCell key={i + 6} id={i + 6} onClick={unlockLetter} />
      ))}
    </div>
  )
}
