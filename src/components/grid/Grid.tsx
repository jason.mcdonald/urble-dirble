import { MAX_CHALLENGES } from '../../constants/settings'
import { isOneWord } from "../../lib/words"
import { CompletedRow } from './CompletedRow'
import { CurrentRow } from './CurrentRow'
import { EmptyRow } from './EmptyRow'
import { RevealRow} from './RevealRow'

type Props = {
  guesses: string[]
  currentGuess: string,
  revealLetter: boolean,
  unlockLetter: (id: number) => void,
}

export const Grid = ({ guesses, currentGuess, revealLetter, unlockLetter }: Props) => {
  let acceptable_challenges = isOneWord() ? MAX_CHALLENGES : MAX_CHALLENGES + 3

  const empties =
    guesses.length < acceptable_challenges - 1
      ? Array.from(Array(acceptable_challenges - 1 - guesses.length))
      : []

  return (
    <div className="pb-6">
      <RevealRow show={revealLetter} unlockLetter={unlockLetter} />
      {guesses.map((guess, i) => (
        <CompletedRow key={i} guess={guess} />
      ))}
      {guesses.length < acceptable_challenges && <CurrentRow guess={currentGuess} />}
      {empties.map((_, i) => (
        <EmptyRow key={i} />
      ))}
    </div>
  )
}
