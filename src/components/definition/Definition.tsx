import { solution, getDefinition } from '../../lib/words'

var levenshtein = require('fast-levenshtein');

const generate_variations = (word: string) => {
  return [word, word + "s", word + "ing", word.substring(0, word.length -1) + "ing", word.substring(0, word.length - 1)]
}

const applyLevenshteinRemovals = (sentence: string, word: string) => {
  let newSentence = ""


  for (let s of sentence.split(' ')) {
    if (levenshtein.get(s, word) <= 2) {
      newSentence = newSentence + "***** "
    } else {
      newSentence = newSentence + s + " "
    }
  }

  return newSentence + "\n"
}

const sanitize_sentence = (sentence: string, word: string) => {
  let sanitized_sentence = applyLevenshteinRemovals(sentence, word)

  for (let w of word.split(' ')) {
    for (let variation of generate_variations(w)) {
      sanitized_sentence = sanitized_sentence.replace(variation, '*****')
    }
  }

  return sanitized_sentence
}

const sanitize_definitions = (definitions: string[], word: string) => {
  let defs = []

  for (let definition of definitions) {
    defs.push(sanitize_sentence(definition.toLowerCase(), word.toLowerCase()))
  }

  return defs
}

type Props = {
  revealWord: boolean,
}


export const Definition = ({ revealWord }: Props) => {

  const definitions = revealWord ? getDefinition(solution) : sanitize_definitions(getDefinition(solution), solution)

  return (
    <div className="items-center justify-center py-10 px-4 text-center sm:block sm:p-0">
      {definitions.map((definition, i) => (
        <>
          <p key={i*6} className="text-sm w-30 text-gray-500 dark:text-gray-300">
            {definition}
          </p>
          <br />
        </>
      ))}
    </div>
  )
}
