import { solution } from './words'

export type CharStatus = 'absent' | 'present' | 'correct' | 'unlocked'

export type CharValue =
  | 'Q'
  | 'W'
  | 'E'
  | 'R'
  | 'T'
  | 'Y'
  | 'U'
  | 'I'
  | 'O'
  | 'P'
  | 'A'
  | 'S'
  | 'D'
  | 'F'
  | 'G'
  | 'H'
  | 'J'
  | 'K'
  | 'L'
  | 'Z'
  | 'X'
  | 'C'
  | 'V'
  | 'B'
  | 'N'
  | 'M'

export const getStatuses = (
  guesses: string[]
): { [key: string]: CharStatus } => {
  const charObj: { [key: string]: CharStatus } = {}

  guesses.forEach((word) => {
    word.split('').forEach((letter, i) => {
      if (!solution.includes(letter)) {
        // make status absent
        return (charObj[letter] = 'absent')
      }

      if (letter === solution[i]) {
        //make status correct
        return (charObj[letter] = 'correct')
      }

      if (charObj[letter] !== 'correct') {
        //make status present
        return (charObj[letter] = 'present')
      }
    })
  })

  return charObj
}

export const getUnlockableLetter = (guesses: string[]): number => {
  let unlockedLetters : number[] = []
  let lockedLetters : number[] = []
  let splitSolution = solution.split('')

  for (let guess of guesses) {
    guess.split('').forEach((letter, i) => {
      if (letter === splitSolution[i] && !unlockedLetters.includes(i)) {
        unlockedLetters.push(i)
      }
    })
  }

  splitSolution.forEach((letter, i) => {
    if (!unlockedLetters.includes(i)) {
      lockedLetters.push(i)
    }
  })

  return lockedLetters[Math.floor(Math.random() * lockedLetters.length)]
}

export const getGuessStatuses = (guess: string): CharStatus[] => {
  const splitSolution = solution.split('')

  const splitGuess = solution.length > 5 ? guess.slice(0,5).split('').concat([' '], guess.slice(5,10).split('')) : guess.split('')

  const solutionCharsTaken = splitSolution.map((_) => false)

  const statuses: CharStatus[] = Array.from(Array(guess.length))

  // handle all correct cases first
  splitGuess.forEach((letter, i) => {
    if (letter === splitSolution[i]) {
      statuses[i] = 'correct'
      solutionCharsTaken[i] = true
      return
    }
  })

  splitGuess.forEach((letter, i) => {
    if (statuses[i]) return

    if (!splitSolution.includes(letter)) {
      // handles the absent case
      statuses[i] = 'absent'
      return
    }

    // now we are left with "present"s
    const indexOfPresentChar = splitSolution.findIndex(
      (x, index) => x === letter && !solutionCharsTaken[index]
    )

    if (indexOfPresentChar > -1) {
      statuses[i] = 'present'
      solutionCharsTaken[indexOfPresentChar] = true
      return
    } else {
      statuses[i] = 'absent'
      return
    }
  })

  return statuses
}
