import { getGuessStatuses } from './statuses'
import { solutionIndex } from './words'
import { GAME_TITLE } from '../constants/strings'
import { MAX_CHALLENGES } from '../constants/settings'
import { solution } from '../lib/words'

export const shareStatus = (guesses: string[], lost: boolean) => {
  let acceptable_challenges = solution.length > 5 ? MAX_CHALLENGES + 3 : MAX_CHALLENGES

  navigator.clipboard.writeText(
    `${GAME_TITLE} ${solutionIndex} ${lost ? 'X' : guesses.length}/${acceptable_challenges}\n\n` +
      generateEmojiGrid(guesses)
  )
}

export const generateEmojiGrid = (guesses: string[]) => {
  return guesses
    .map((guess) => {
      const status = getGuessStatuses(guess)
      return guess
        .split('')
        .map((_, i) => {
          switch (status[i]) {
            case 'correct':
              return '🟦'
            case 'present':
              return '🟧'
            default:
              return '⬜'
          }
        })
        .join('')
    })
    .join('\n')
}
