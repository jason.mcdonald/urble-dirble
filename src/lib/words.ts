import { WORDS } from '../constants/wordlist'
import { VALIDGUESSES } from '../constants/validGuesses'
import { DEFINITIONS } from '../constants/definitions'


export const getDefinition = (word: string) => {
  for (let row of DEFINITIONS) {
    let splitRow = row.split('::')
    if (splitRow[0].toLowerCase() === word.toLowerCase()) {
      return splitRow.slice(1)
    }
  }

  return [""]
}

export const isOneWord = (): boolean => {
  return getWordOfDay().solution.length <= 5
}

export const isWordInWordList = (word: string) => {

  if (isOneWord()) {
    return (
      WORDS.includes(word.toLowerCase()) ||
      VALIDGUESSES.includes(word.toLowerCase())
    )
  }

  let firstWord = word.slice(0,5)
  let secondWord = word.slice(5,10)

  return (
    (WORDS.includes(firstWord.toLowerCase()) && WORDS.includes(secondWord.toLowerCase())) ||
    (VALIDGUESSES.includes(firstWord.toLowerCase()) && VALIDGUESSES.includes(secondWord.toLowerCase()))
  )
}

export const isWinningWord = (word: string) => {
  if (isOneWord()) {
    return solution === word
  }

  let guess = word.slice(0,5) + ' ' + word.slice(5,10)
  return solution === guess
}

export const getWordOfDay = () => {
  // January 1, 2022 Game Epoch
  const epochMs = new Date('January 1, 2022 00:00:00').valueOf()
  const now = Date.now()
  const msInDay = 86400000
  const index = Math.floor((now - epochMs) / msInDay)
  const nextday = (index + 1) * msInDay + epochMs

  return {
    solution: WORDS[index % WORDS.length].toUpperCase(),
    solutionIndex: index,
    tomorrow: nextday,
  }
}

export const getDay = (): number => {
  const epochMs = new Date('January 1, 2022 00:00:00').valueOf()
  const now = Date.now()
  const msInDay = 86400000
  const index = Math.floor((now - epochMs) / msInDay)

  return index
}

export const { solution, solutionIndex, tomorrow } = getWordOfDay()
